package com.moonlite.http;

import java.io.IOException;

public interface IHttpRequest {
    String get(String url) throws IOException;
    String get(String url, String user, String password) throws IOException;
}
