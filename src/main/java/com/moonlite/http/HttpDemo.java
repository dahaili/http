package com.moonlite.http;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/***
 * 
 * @author Dahai Li
 * 
 * Learn how to write program to do http request.
 * @see URL, URLConnection
 *
 */
public class HttpDemo {
    public static void main(String[] args) throws IOException, KeyManagementException, NoSuchAlgorithmException  {
//        String result = new Oracle().get("https://asav/admin/config", "asadp", "dat@package");
        String result = new Oracle().get("http://www.google.com");
        System.out.println(result);
    }
}
