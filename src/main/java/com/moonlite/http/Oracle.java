package com.moonlite.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.DatatypeConverter;

/**
 * Use JDK's HttpURLConnection for GET method
 * @author dli
 *
 */
public class Oracle implements IHttpRequest {
    /***
     * Use URLConnection from JDK
     * 
     * @param path String
     * @return String
     * @throws IOException
     */
    public String get(String path) throws IOException {
        final URL url = new URL(path);
        final URLConnection urlConnection = url.openConnection();
        HttpURLConnection httpConn = null;
        if (urlConnection instanceof HttpURLConnection) {
            httpConn = (HttpURLConnection) urlConnection;
            StringBuffer result = new StringBuffer();
            final BufferedReader in = new BufferedReader(new InputStreamReader(
                    httpConn.getInputStream()));
            for (String line = in.readLine(); line != null; line = in
                    .readLine()) {
                result.append(line + '\n');
            }
            in.close();
            return result.toString();
        } else {
            return "URL is not valid http URL";
        }
    }

    public String get(String path, String user, String password) throws IOException {
        final URL url = new URL(path);
        final HttpsURLConnection httpsConn = (HttpsURLConnection) url.openConnection();
        setAuthenticationProperty(httpsConn, user, password);
        try {
            bypassCertChecking(httpsConn);
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
        final BufferedReader in = new BufferedReader(new InputStreamReader(
                httpsConn.getInputStream()));
        StringBuffer result = new StringBuffer();
        for (String line = in.readLine(); line != null; line = in
                .readLine()) {
            result.append(line + '\n');
        }
        in.close();
        return result.toString();
    }

    private void bypassCertChecking(HttpsURLConnection httpsConn) throws NoSuchAlgorithmException, KeyManagementException {
        final SSLContext ssl = SSLContext.getInstance("SSL");
        ssl.init(null, new TrustManager[]{new BypassX509TrustManager()}, null);
        httpsConn.setSSLSocketFactory(ssl.getSocketFactory());
        httpsConn.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String urlHost, SSLSession ssls) {
                return true;
            }
        });
    }

    private void setAuthenticationProperty(HttpsURLConnection httpsConn, String user, String password) {
        final String userpass = user + ":" + password;
        final String basicAuth = "Basic " + DatatypeConverter.printBase64Binary(userpass.getBytes());
        httpsConn.setRequestProperty("Authorization", basicAuth);
    }

    /**
     * A simpler version of HTTP reader using URLConnection
     * 
     * @param path String
     * @throws IOException
     */
    public void get2(String path) throws IOException {
        final URL url = new URL(path);
        final BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
        for (String line = in.readLine(); line != null; line = in.readLine()) {
            System.out.println(line);
        }
        in.close();
    }

    private static class BypassX509TrustManager implements X509TrustManager {
        @Override
        public void checkClientTrusted(X509Certificate[] cert, String s) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] cert, String s) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
}